package com.study.editor.controller

import com.study.editor.controller.SchemeController.SchemeEditOperation.*
import com.study.editor.graphics.toPoint
import com.study.editor.service.XmlService
import javafx.embed.swing.SwingFXUtils
import javafx.fxml.FXML
import javafx.geometry.Point2D
import javafx.scene.Parent
import javafx.scene.SnapshotParameters
import javafx.scene.control.Alert
import javafx.scene.control.Alert.AlertType
import javafx.scene.input.MouseEvent
import javafx.scene.layout.AnchorPane
import javafx.stage.FileChooser
import tornadofx.View
import java.io.IOException
import javax.imageio.ImageIO


class SchemeController : View("Algogram") {

    override val root: Parent by fxml(location = "/view.fxml", hasControllerAttribute = true)

    @FXML
    private lateinit var workPane: AnchorPane

    init {
        State.workSpace = workPane.children
    }

    private fun setOperation(schemeEditOperation: SchemeEditOperation) {
        State.operation = schemeEditOperation
    }

    private fun addStartEndBlock(point: Point2D) {
        val block = State.getBlock(point)
        val line = block.getInsertableLines().get(0)
        val list = listOf(block.getTopShape(), block.getBottomShape())
        State.insertLineList.add(line)
        State.shapeList.addAll(list)
        State.workSpace.addAll(line.getElements() + list)
    }

    fun aboutDialog() {
        val alert = Alert(AlertType.INFORMATION)
        alert.dialogPane.setPrefSize(400.0, 300.0)
        alert.headerText = "Algogram - редактор блок-схем"
        alert.contentText = "Copyright (c) Бутилов Д.В., Желтов М.И., 2018"
        alert.showAndWait()
    }

    fun saveAsPng() {
        val fileChooser = FileChooser()
        fileChooser.title = "Save Image"
        fileChooser.extensionFilters.add(FileChooser.ExtensionFilter("PNG", "*.png"))
        val file = fileChooser.showSaveDialog(null)
        if (file != null) {
            try {
                val snapshot = workPane.snapshot(SnapshotParameters(), null)
                ImageIO.write(SwingFXUtils.fromFXImage(snapshot, null), "png", file)
            } catch (ex: IOException) {
                println(ex.message)
            }
        }
    }

    fun newScheme() = State.clearScheme()

    fun saveAsXml() {
        val fileChooser = FileChooser()
        fileChooser.title = "Save file"
        fileChooser.extensionFilters.add(FileChooser.ExtensionFilter("XML", "*.xml"))
        val file = fileChooser.showSaveDialog(null)
        if (file != null) {
            XmlService.createXmlFile(file)
        }
    }

    fun openXml() {
        val fileChooser = FileChooser()
        fileChooser.title = "Open file"
        fileChooser.extensionFilters.add(FileChooser.ExtensionFilter("XML", "*.xml"))
        val file = fileChooser.showOpenDialog(null)
        if (file != null) {
            XmlService.openXmlFile(file)
            State.hasStartEndBlock = true
        }
    }

    fun clickPane(mouseEvent: MouseEvent) {
        // конченный костыль, потому что mouseEvent.isPrimaryButtonDown не работает :(
        if (mouseEvent.button.name.equals("PRIMARY") && State.isReadyToStart()) {
            addStartEndBlock(mouseEvent.toPoint())
            State.hasStartEndBlock = true
        }
    }

    fun MenuClose() = close()

    fun chooseSelectionMode() {
        State.isSelectionMode = !State.isSelectionMode
        State.insertLineList.forEach { it.stateChanged(State.isSelectionMode) }
    }

    fun chooseOperationAddStartBlock() = setOperation(AddBeginEndBlock)
    fun chooseOperationAddIoBlock() = setOperation(AddIoBlock)
    fun chooseOperationAddProcessBlock() = setOperation(AddProcessBlock)
    fun chooseOperationAddAssignmentBlock() = setOperation(AddAssignmentBlock)
    fun chooseOperationAddBranchingBlock() = setOperation(AddBranchingBlock)
    fun chooseOperationAddCycleBlock() = setOperation(AddCycleBlock)

    enum class SchemeEditOperation {
        AddBeginEndBlock,
        AddIoBlock,
        AddProcessBlock,
        AddAssignmentBlock,
        AddBranchingBlock,
        AddCycleBlock;
    }

    fun chooseOperationClearScheme() = State.clearScheme()
}