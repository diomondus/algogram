package com.study.editor.controller

import com.study.editor.blocks.FlowBlock
import com.study.editor.blocks.FlowBlockFactory
import com.study.editor.controller.SchemeController.SchemeEditOperation
import com.study.editor.controller.SchemeController.SchemeEditOperation.*
import com.study.editor.graphics.FlowInsertableLine
import com.study.editor.graphics.FlowShape
import com.study.editor.service.IdService
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import javafx.geometry.Point2D
import javafx.scene.Node
import javafx.scene.paint.Color

object State {
    lateinit var workSpace: ObservableList<Node>

    internal var insertLineList = FXCollections.observableArrayList<FlowInsertableLine>()
    internal var shapeList = FXCollections.observableArrayList<FlowShape>()
    val idService: IdService = IdService()

    var isSelectionMode = false
    var hasStartEndBlock = false
    var operation: SchemeEditOperation = AddBeginEndBlock

    var figureColor = Color.WHITESMOKE

    fun isReadyForInsert(): Boolean = !isSelectionMode && operation != AddBeginEndBlock
    fun isReadyToStart(): Boolean = !isSelectionMode && !hasStartEndBlock

    fun clearScheme() {
        workSpace.clear()
        shapeList.clear()
        insertLineList.clear()
        hasStartEndBlock = false
    }

    fun getBlock(point: Point2D): FlowBlock {
        if (hasStartEndBlock) {
            when (operation) {
                AddIoBlock -> return FlowBlockFactory.getIOBlock(point)
                AddProcessBlock -> return FlowBlockFactory.getProcessBlock(point)
                AddAssignmentBlock -> return FlowBlockFactory.getAssignmentBlock(point)
                AddBranchingBlock -> return FlowBlockFactory.getBranchingBlock(point)
                AddCycleBlock -> return FlowBlockFactory.getCycleBlock(point)
                AddBeginEndBlock -> {
                    if (!workSpace.isEmpty()) {
                        clearScheme()
                    }
                    return FlowBlockFactory.getStartEndBlock(point)
                }
            }
        } else {
            return FlowBlockFactory.getStartEndBlock(point)
        }
    }

    fun generateId(node: Node): String = idService.generateID(node)
}