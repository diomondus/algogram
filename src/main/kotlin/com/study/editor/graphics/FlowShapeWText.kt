package com.study.editor.graphics

import javafx.event.EventHandler
import javafx.geometry.Pos
import javafx.scene.control.TextField
import javafx.scene.input.KeyCode
import javafx.scene.text.Text
import javafx.scene.text.TextAlignment
import tornadofx.*

abstract class FlowShapeWText(text: String) : FlowShape() {

    protected val textField = TextField(text)
    protected val textView = Text(text)

    open fun getMaxSimbols() = 16

    init {
        textView.textAlignment = TextAlignment.CENTER
        textField.alignment = Pos.CENTER
        onDoubleClick {
            textView.text = textField.text
            children.add(textField)
            textField.requestFocus()
        }
        textField.onKeyPressed = EventHandler { keyEvent ->
            if (keyEvent.code == KeyCode.ENTER) {
                textView.text = textField.text
                if (textView.text.length > getMaxSimbols() + 3) {
                    textView.text = textView.text.substring(0, getMaxSimbols()) + "..."
                }
                children.remove(textField)
            }
        }
        textField.setMaxSize(160.0, 0.0) // todo(получение ширины фигуры)
    }
}