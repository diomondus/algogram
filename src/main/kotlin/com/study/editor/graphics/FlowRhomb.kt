package com.study.editor.graphics

import com.study.editor.controller.State
import com.study.editor.blocks.BranchingBlock
import com.study.editor.blocks.CycleBlock
import javafx.geometry.Point2D
import javafx.scene.shape.Line
import javafx.scene.shape.Polygon
import org.w3c.dom.Document
import org.w3c.dom.Element

class FlowRhomb(point: Point2D, val rWidth: Double, val rHeight: Double, val label: String) : FlowShapeWText(label) {

    private val rhomb: Polygon

    override fun getMaxSimbols(): Int {
        return super.getMaxSimbols() - 5
    }

    init {
        val x = point.x
        val y = point.y
        val halfW = rWidth / 2
        val halfH = rHeight / 2

        translateX = x - halfW
        translateY = y - halfH

        val yphH = y + halfH
        rhomb = Polygon(x, y, x + halfW, yphH, x, y + rHeight, translateX, yphH, x, y)
        rhomb.fill = State.figureColor
        rhomb.stroke = State.figureColor
        rhomb.strokeWidth = 2.0

        setGlowableOnMouseEnter()

        children.addAll(rhomb, textView)

        textField.setMaxSize(100.0, 0.0) // todo(получение ширины фигуры)
    }

    override fun getXmlData(doc: Document): Element {
        val createElement = doc.createElement("Object")
        if (flowBlock.javaClass.simpleName.equals("BranchingBlock")) {
            createElement.setAttribute("type", "FlowBranchingRhomb")
        } else {
            createElement.setAttribute("type", "FlowCycleRhomb")
        }

        createElement.setAttribute("name", id)
        val w = translateX + rWidth / 2
        createElement.setAttribute("X", w.toString())
        val y = translateY + rHeight / 2
        createElement.setAttribute("Y", y.toString())
        createElement.setAttribute("width", rWidth.toString())
        createElement.setAttribute("height", rHeight.toString())
        createElement.appendChild(doc.createTextNode(label))

        val items = flowBlock.getItems() //вытащим все левые линии
        for (insertableLine in flowBlock.getInsertableLines()) { //чистка от линий со вставкой. остались пустые линии и невидимые кружки
            items.remove(insertableLine.line)
            items.remove(insertableLine.circle)
        }
        for (item in items) {
            if (item.typeSelector.equals("Line")) {
                val lineElement = doc.createElement("Line")

                item as Line
                lineElement.setAttribute("type", item.typeSelector)
                lineElement.setAttribute("X1", item.startXProperty().get().toString())
                lineElement.setAttribute("Y1", item.startYProperty().get().toString())
                lineElement.setAttribute("X2", item.endXProperty().get().toString())
                lineElement.setAttribute("Y2", item.endYProperty().get().toString())

                createElement.appendChild(lineElement)
            }
        }
        if (flowBlock.javaClass.simpleName.equals("BranchingBlock")) {
            var xmlData = (flowBlock as BranchingBlock).circleTopLeft.getXmlData(doc)
            xmlData.setAttribute("type1", "circleTopLeft")
            createElement.appendChild(xmlData)
            xmlData = (flowBlock as BranchingBlock).circleTopRight.getXmlData(doc)
            xmlData.setAttribute("type1", "circleTopRight")
            createElement.appendChild(xmlData)
            xmlData = (flowBlock as BranchingBlock).circleBottomLeft.getXmlData(doc)
            xmlData.setAttribute("type1", "circleBottomLeft")
            createElement.appendChild(xmlData)
            xmlData = (flowBlock as BranchingBlock).circleBottomRight.getXmlData(doc)
            xmlData.setAttribute("type1", "circleBottomRight")
            createElement.appendChild(xmlData)
            xmlData = (flowBlock as BranchingBlock).circleBottomCenter.getXmlData(doc)
            xmlData.setAttribute("type1", "circleBottomCenter")
            createElement.appendChild(xmlData)
        } else {
            var xmlData = (flowBlock as CycleBlock).circleTopRight.getXmlData(doc)
            xmlData.setAttribute("type1", "circleTopRight")
            createElement.appendChild(xmlData)
            xmlData = (flowBlock as CycleBlock).circleBottomCenter.getXmlData(doc)
            xmlData.setAttribute("type1", "circleBottomCenter")
            createElement.appendChild(xmlData)
            xmlData = (flowBlock as CycleBlock).circleBottomRight.getXmlData(doc)
            xmlData.setAttribute("type1", "circleBottomRight")
            createElement.appendChild(xmlData)
            xmlData = (flowBlock as CycleBlock).circleTopLeft.getXmlData(doc)
            xmlData.setAttribute("type1", "circleTopLeft")
            createElement.appendChild(xmlData)
            xmlData = (flowBlock as CycleBlock).circleCenterLeft.getXmlData(doc)
            xmlData.setAttribute("type1", "circleCenterLeft")
            createElement.appendChild(xmlData)
            xmlData = (flowBlock as CycleBlock).circleCenter.getXmlData(doc)
            xmlData.setAttribute("type1", "circleCenter")
            createElement.appendChild(xmlData)
            xmlData = (flowBlock as CycleBlock).circleCenterTop.getXmlData(doc)
            xmlData.setAttribute("type1", "circleCenterTop")
            createElement.appendChild(xmlData)
        }

        return createElement
    }
}