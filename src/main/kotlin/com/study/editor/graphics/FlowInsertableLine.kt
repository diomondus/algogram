package com.study.editor.graphics

import com.study.editor.controller.State
import javafx.collections.ObservableList
import javafx.event.EventHandler
import javafx.scene.Node
import javafx.scene.paint.Color
import javafx.scene.shape.Circle
import javafx.scene.shape.Shape

class FlowInsertableLine(shape1: FlowShape, shape2: FlowShape, var parentList: ObservableList<Node>) : FlowLine(shape1, shape2) {

    val circle = Circle(9.0)

    init {
        children.add(circle)

        stateChanged(State.isSelectionMode)
        circle.centerXProperty().bind((line.startXProperty().add(line.endXProperty()).divide(2)))
        circle.centerYProperty().bind((line.startYProperty().add(line.endYProperty()).divide(2)))

        circle.onMouseClicked = EventHandler { insertShape() }
    }

    fun getElements(): List<Shape> = listOf(line, circle)

    fun stateChanged(isSelectionMode: Boolean) {
        if (isSelectionMode) {
            deleteCircle()
        } else {
            renderCircle()
        }
    }

    private fun insertShape() {
        if (State.isReadyForInsert()) {
            val point = circle.toPoint()
            val flowBlock = State.getBlock(point)

            val topShape: FlowShape = flowBlock.getTopShape()
            val bottomShape: FlowShape = flowBlock.getBottomShape()
            topShape.flowBlock = flowBlock
            //shape1  - верхний элемент, shape2 - нижний элемент в связке
            val line1 = FlowInsertableLine(shape1, topShape, parentList) // новая линия, связует верхний элемент и новый
            val line2 = FlowInsertableLine(bottomShape, shape2, parentList) // новый и нижний

            // зададим фигуре ссылки на линии
            topShape.line1 = line1
            topShape.line2 = line2
            bottomShape.line2 = line2

            // обновим ссылки на линии у старых фигур
            shape1.line2 = line1
            shape2.line1 = line2
            //для ромба
            if (shape1.typeSelector.equals("FlowCircle"))//это линия до точки движения. она есть только у ромба
                for (node in parentList) {
                    if (node.typeSelector.equals("FlowRhomb")) {
                        //здесь берешь линии и сравниваешь
                        if ((node as FlowShape).line2.equals(this)) {
                            //ромбу нужно присвоить новую линию line1
                            node.line2 = line1
                            break
                        }
                    }
                }

            State.shapeList.add(topShape)
            if (topShape != bottomShape) {
                State.shapeList.add(bottomShape)
            }
            State.insertLineList.addAll(line1, line2)
            State.insertLineList.addAll(flowBlock.getInsertableLines())
            State.insertLineList.remove(this)

            val elements = line1.getElements() + line2.getElements() + flowBlock.getItems()
            State.workSpace.removeAll(getElements())
            State.workSpace.addAll(elements)
            parentList.removeAll(getElements())
            parentList.add(topShape)
            parentList.addAll(elements)

            // удалим и добавим все фигуры, чтобы линии оказались под фигурами
            State.workSpace.removeAll(State.shapeList)
            State.workSpace.addAll(State.shapeList)
        }
    }

    private fun deleteCircle() {
        circle.fill = Color.TRANSPARENT
        circle.stroke = Color.TRANSPARENT
    }

    private fun renderCircle() {
        circle.fill = Color.ORANGE
    }
}