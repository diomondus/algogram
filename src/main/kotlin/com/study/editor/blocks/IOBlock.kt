package com.study.editor.blocks

import com.study.editor.graphics.FlowInsertableLine
import com.study.editor.graphics.FlowParallelogram
import com.study.editor.graphics.FlowShape
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import javafx.geometry.Point2D
import javafx.scene.Node

class IOBlock(point: Point2D, rWidth: Double, rHeight: Double, label: String) : FlowBlock {
    val flowParallelogram = FlowParallelogram(point, rWidth, rHeight, label)

    override fun getTopShape(): FlowShape = flowParallelogram

    override fun getBottomShape(): FlowShape = flowParallelogram

    override fun getInsertableLines(): List<FlowInsertableLine> = emptyList()

    override fun getItems(): ObservableList<Node> = FXCollections.observableArrayList()

    override fun getShapes(): ObservableList<FlowShape> = FXCollections.observableArrayList()
}