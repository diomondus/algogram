package com.study.editor.blocks

import com.study.editor.graphics.FlowInsertableLine
import com.study.editor.graphics.FlowShape
import javafx.collections.ObservableList
import javafx.scene.Node

interface FlowBlock {

    fun getTopShape(): FlowShape

    fun getBottomShape(): FlowShape

    fun getInsertableLines(): List<FlowInsertableLine>

    fun getItems(): ObservableList<Node>

    fun getShapes(): ObservableList<FlowShape>
}