package com.study.editor.blocks

import com.study.editor.graphics.*
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import javafx.geometry.Point2D
import javafx.scene.Node
import tornadofx.*

class CycleBlock : FlowBlock {

    val flowRhomb: FlowRhomb

    // right part
    val circleTopRight: FlowCircle
    val circleBottomCenter: FlowCircle
    val circleBottomRight: FlowCircle

    val lineRhombToTr: FlowLineFromRhomb
    val lineTrBr: FlowLine
    val lineBrBc: FlowLine

    // left part
    val circleTopLeft: FlowCircle
    val circleCenterLeft: FlowCircle
    val circleCenter: FlowCircle
    val circleCenterTop: FlowCircle

    val lineRhombToTl: FlowLineToRhomb
    val lineTlToCl: FlowLine
    val lineClToC: FlowLine
    val lineCToCT: FlowInsertableLine
    val lineRhombToCT: FlowLine

    var list: ObservableList<Node> = FXCollections.observableArrayList()

    constructor(point: Point2D, rWidth: Double, rHeight: Double, label: String) {
        flowRhomb = FlowRhomb(point, rWidth, rHeight, label)
        val x = point.x
        val y = point.y
        val radius = 6.0

        val lineLength = 150
        val shift = 50

        val ypl = y + lineLength
        val yplps = ypl + shift
        val xpl = x + lineLength

        // right part
        circleTopRight = FlowCircle(xpl, y, radius)
        circleBottomRight = FlowCircle(xpl, yplps, radius)
        circleBottomCenter = FlowCircle(x, yplps, radius)

        lineRhombToTr = FlowLineFromRhomb(flowRhomb, circleTopRight)
        lineTrBr = FlowLine(circleTopRight, circleBottomRight)
        lineBrBc = FlowLine(circleBottomRight, circleBottomCenter)

        // left part
        val xml = x - lineLength - radius
        circleTopLeft = FlowCircle(xml, y, radius)
        circleCenterLeft = FlowCircle(xml, ypl, radius)
        circleCenter = FlowCircle(x + 2, ypl, radius)
        circleCenterTop = FlowCircle(x + 2, y + shift, radius)

        lineRhombToTl = FlowLineToRhomb(circleTopLeft, flowRhomb)
        lineTlToCl = FlowLine(circleCenterLeft, circleTopLeft)
        lineClToC = FlowLine(circleCenter, circleCenterLeft)
        lineCToCT = FlowInsertableLine(circleCenterTop, circleCenter, list)
        lineRhombToCT = FlowLine(flowRhomb, circleCenterTop)
        lineRhombToCT.line.startYProperty().bind(flowRhomb.translateYProperty().add(flowRhomb.heightProperty().minus(2)))

        // add all items
        list.addAll(listOf(circleTopLeft, circleCenterLeft, circleCenter, circleCenterTop, circleTopRight,
                circleBottomRight, circleBottomCenter, lineRhombToTl.line, lineTlToCl.line, lineClToC.line,
                lineRhombToCT.line, lineRhombToTr.line, lineTrBr.line, lineBrBc.line) + lineCToCT.getElements())
    }

    constructor(point: Point2D, rWidth: Double, rHeight: Double, label: String, actr: FlowCircle, actl: FlowCircle,
                accl: FlowCircle, acc: FlowCircle, acct: FlowCircle, acbr: FlowCircle, acbc: FlowCircle) {
        flowRhomb = FlowRhomb(point, rWidth, rHeight, label)
        val x = point.x
        val y = point.y
        val radius = 6.0

        val lineLength = 150
        val shift = 50

        val ypl = y + lineLength
        val yplps = ypl + shift
        val xpl = x + lineLength

        // right part
        circleTopRight = actr
        circleBottomRight = acbr
        circleBottomCenter = acbc

        lineRhombToTr = FlowLineFromRhomb(flowRhomb, circleTopRight)
        lineTrBr = FlowLine(circleTopRight, circleBottomRight)
        lineBrBc = FlowLine(circleBottomRight, circleBottomCenter)

        // left part
        val xml = x - lineLength - radius
        circleTopLeft = actl
        circleCenterLeft = accl
        circleCenter = acc
        circleCenterTop = acct

        lineRhombToTl = FlowLineToRhomb(circleTopLeft, flowRhomb)
        lineTlToCl = FlowLine(circleCenterLeft, circleTopLeft)
        lineClToC = FlowLine(circleCenter, circleCenterLeft)
        lineCToCT = FlowInsertableLine(circleCenterTop, circleCenter, list)
        lineRhombToCT = FlowLine(flowRhomb, circleCenterTop)
        lineRhombToCT.line.startYProperty().bind(flowRhomb.translateYProperty().add(flowRhomb.heightProperty().minus(2)))

        // add all items
        list.addAll(listOf(circleTopLeft, circleCenterLeft, circleCenter, circleCenterTop, circleTopRight,
                circleBottomRight, circleBottomCenter, lineRhombToTl.line, lineTlToCl.line, lineClToC.line,
                lineRhombToCT.line, lineRhombToTr.line, lineTrBr.line, lineBrBc.line) + lineCToCT.getElements())
    }

    override fun getTopShape(): FlowShape = flowRhomb

    override fun getBottomShape(): FlowShape = circleBottomCenter

    override fun getInsertableLines(): List<FlowInsertableLine> = listOf(lineCToCT)

    override fun getItems(): ObservableList<Node> = list

    override fun getShapes(): ObservableList<FlowShape> {
        return FXCollections.emptyObservableList()
    }
}